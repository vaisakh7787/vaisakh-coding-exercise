package com.gamesys.roulette;
import java.util.concurrent.BlockingQueue;

import org.junit.Test;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Assert;

public class BetAnalyzerTest {
	BetManager betManager = null;
	BlockingQueue<Integer> betRsultQueue = null;
	RouletteDAO userDAO = null;
	
    @Before
    public void setUp() {
    	betManager = new BetManager(null);
    	BetObject bO = new BetObject("testUser", "EVEN", 100.50);
    	betManager.addActiveBets(bO);
    	
    	RouletteUser ru = new RouletteUser();
    	ru.setUserName("testUser");
    	ru.setTotalBet(10.0);
    	ru.setTotalWin(100.0);
    	
    	RouletteDAO userDAO = RouletteDAO.getInstance();
    	userDAO.addUser(ru);    	
	}	
	
   @Test
   public void testAnalyzeBet() {
	   BetAnalyzer betA = new BetAnalyzer(betManager, betRsultQueue);
	   betA.analyzeBet(10);
	   System.out.println(userDAO.getInstance().getUser("testUser").getTotalWin());
	   Double val = userDAO.getInstance().getUser("testUser").getTotalWin();
	   Assert.assertEquals(String.valueOf(301D), String.valueOf(val));
   }
}