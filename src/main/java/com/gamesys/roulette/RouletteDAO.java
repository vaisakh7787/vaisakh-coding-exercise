/**
 * 
 */
package com.gamesys.roulette;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * @author vaisakh
 *
 */
public class RouletteDAO {

	private static final String INVALID_FILE_ERROR = 
			"Invalid input file. Please re-enter the correct input file path";
	private static RouletteDAO daoObj = null;
	private ArrayList<RouletteUser> rouletteUsers = new ArrayList<RouletteUser>();
	
	public static RouletteDAO getInstance() {
		if (daoObj == null ) {
			daoObj = new RouletteDAO();
		}		
		return daoObj;
	}
	
	private RouletteDAO() {		
	}
	
	public void initializeUsers () {
		Scanner scanIn = null;
		try {
			scanIn = new Scanner(System.in);
			while (true) {
				System.out.println("Please enter the input file with player information - ");	    	
				String inputFileName = scanIn.nextLine();	    
				boolean isValidFile = getUserInfoFromFile(inputFileName);
				if (isValidFile) break;
				System.out.println(INVALID_FILE_ERROR);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			//if (scanIn != null ) scanIn.close();
		}
	}
	
	private boolean getUserInfoFromFile(String fileName){
		File file = new File(fileName);
		boolean isValidFile = true;
		if(!(file.exists())) {			
			return false;
		}
		
		BufferedReader reader =null; 
		try {
			reader = new BufferedReader(
									new InputStreamReader(new FileInputStream(file)));
			String line;					
			while ((line = reader.readLine())!= null) {
				RouletteUser rU = new RouletteUser();
				StringTokenizer tokenizer = new StringTokenizer(line);
				int tCount = 0;
				
				while (tokenizer.hasMoreTokens()) {
					String value = tokenizer.nextToken(",");
										
					if(tCount == 0) {
						rU.setUserName(value);
						isValidFile = true;
					}
					else if(tCount == 1) rU.setTotalWin(Double.parseDouble(value));
					else if(tCount == 2) rU.setTotalBet(Double.parseDouble(value));
					else break;
					
					tCount++;
				}	
				
				this.addUser(rU);
				System.out.println(rU.toString());
			}
			
			if(!(isValidFile)) return false;						
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			return false;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		} finally {
			try {
				if(reader != null) reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return true;
	}
	
	public void updateWinnings(String userName , Double betAmount, Double winnings) {
		RouletteUser user = getUser(userName);
		if (user != null) {
			user.setTotalBet(user.getTotalBet() + betAmount);
			user.setTotalWin(user.getTotalWin()+ winnings);
		}
	}

	private ArrayList<RouletteUser> getRouletteUsers() {
		return rouletteUsers;
	}
	
	public RouletteUser getUser(String userName){
		for (RouletteUser user : this.getRouletteUsers()) {
			if(user.getUserName().equals(userName)) return user;
		}
		
		return null;
	}
	
	//exposing with public for Junit
	public void addUser (RouletteUser user) {
		this.getRouletteUsers().add(user);
	}
	
	public boolean verifyUser (String userName) {
		for( RouletteUser user : this.getRouletteUsers()) {
			if (userName.equals(user.getUserName())) {				
				return true;
			}
		}		
		return false;
	}
	
	public void printUserStatus() {
		
		System.out.println("----------------- TOTAL WINNINGS ----------------");
		System.out.println("|Player              |Total Win       |Total Bet");
		System.out.println("-------------------------------------------------");
        
		for (RouletteUser user : this.getRouletteUsers()) {
			System.out.print("|"+user.getUserName());            		
			Util.alignSpaces(user.getUserName() , 20);
			System.out.print("|"+user.getTotalWin());            		
			Util.alignSpaces(String.valueOf(user.getTotalWin()),17);
			System.out.print("|"+user.getTotalBet());	
			System.out.println("\n");
		}
	}
}
