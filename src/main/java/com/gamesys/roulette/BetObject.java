package com.gamesys.roulette;

public class BetObject {

	String userName;
	String betOn;
	Double amount;
	
	public BetObject(String userName, String betOn, Double amount) {
		this.userName = userName;
		this.betOn = betOn;
		this.amount = amount;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getBetOn() {
		return betOn;
	}
	public void setBetOn(String betOn) {
		this.betOn = betOn;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "Bet placed successfully. (Player Name=" + userName + ", Bet On=" + betOn
				+ ", Bet Amount=" + amount + "]";
	}	
	
}
