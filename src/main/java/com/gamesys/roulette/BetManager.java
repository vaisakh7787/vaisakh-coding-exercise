package com.gamesys.roulette;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.concurrent.BlockingQueue;

public class BetManager implements Runnable{
	RouletteDAO rDAO = null;
		
	ArrayList<BetObject> activeBets = new ArrayList<BetObject>();
	BlockingQueue<Integer> betResultQueue = null;
	
	public BetManager(BlockingQueue<Integer> betResultQueue) {
		this.betResultQueue = betResultQueue;
	}
	
	public void startBets() {
		rDAO = rDAO.getInstance();
		Thread betThread = new Thread(this);
		betThread.start();
	}

	@Override
	public void run() {
		scanInput();		
	}
	
	private void scanInput() {
		Scanner scanIn = null;
		try {
			scanIn = new Scanner(System.in);
			while (true) {
				System.out.println("\n");
				System.out.println("Place your bets - (Format -> Name,1-36|EVEN|ODD,Amount) . Anytime press 't' for Total winnings , 'r' for the time left to bet and 'e' to exit game.");	    	
				String inputBet = scanIn.nextLine();	    
				if (inputBet.equals("t")) {
					rDAO.printUserStatus();
					continue;
				}else if (inputBet.equals("r")) {
					System.out.println(RouletteRunner.spinTimer + " Seconds");
					continue;
				}else if (inputBet.equals("e")) {
					System.out.println("Good Bye!");
					System.exit(0);
				}				
				boolean isValidBet = parseBet(inputBet);				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (scanIn != null ) scanIn.close();
		}
	}
	
	public boolean parseBet(String inputBet) {
		StringTokenizer tokenizer = new StringTokenizer(inputBet,",");
		int tCount = 0;
		
		String userName = null ;
		String betOnS = null;
		Integer betOnD = null;
		Double value = null;
		
		if(tokenizer.countTokens() != 3) {
			System.out.println("Invalid bet. Please try again with the correct format.");
			return false;
		}
		
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken(",");								
			if(tCount == 0) {
				boolean isValidUser = rDAO.verifyUser(token);
				if(!isValidUser) {
					System.out.println("Player not avaialble. Please enter bets for an active player");
					return false;
				}
				userName = token;				
			} else if(tCount == 1) {
				try {
					betOnD = Integer.parseInt(token);
					if(betOnD>36 || betOnD<1) {
						System.out.println("Invalid bet entry. Please bet on a number between 1 and 36");
						return false;
					}
				}catch(NumberFormatException ne) {
					if (token.equals("EVEN") || token.equals("ODD")){
						betOnS = token;						
					} else {
						System.out.println("Invalid bet entry. Please try again.");
						return false;
					}
				}
			} else if(tCount == 2) {
				try {
					value = Double.parseDouble(token);					
				}catch(NumberFormatException ne) {
					System.out.println("Invalid bet amount. Please try again.");		
					return false;
				}
			}			
			else break;			
			tCount++;
		}
		
		BetObject betO = new BetObject(userName,
									  ((betOnS != null)?betOnS:String.valueOf(betOnD)),
										value);
				
		if(doesBetExistForUser(userName)) {
			System.out.println("Bets already placed and cannot be modified. - "+ userName);			
		} else {
			addActiveBets(betO);
			System.out.println(betO.toString());
		}
		
		return true;		
	}	
	
	//exposing with public for Junit
	public synchronized void addActiveBets(BetObject betO) {
		this.activeBets.add(betO);
	}
	
	public synchronized void clearActiveBets(){
		activeBets.clear();
	}
	
	public synchronized ArrayList<BetObject> getActiveBets() {
		return activeBets;
	}
	
	private synchronized boolean doesBetExistForUser(String userName){
		for(BetObject bO: getActiveBets()) {
			if (userName.equals(bO.getUserName())){
				return true;
			}
		}		
		return false;
	}
}
