package com.gamesys.roulette;

import java.util.concurrent.BlockingQueue;

public class BetAnalyzer implements Runnable{

	BetManager betManager = null;
	BlockingQueue<Integer> betRsultQueue = null;
	RouletteDAO userDAO = null;
	public BetAnalyzer(BetManager betManager, BlockingQueue<Integer> betResultQueue) {
		this.betManager = betManager;
		this.betRsultQueue = betResultQueue;
		userDAO = RouletteDAO.getInstance();
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true){
	        try {
	            Integer lotValue = betRsultQueue.take();	            
	            System.out.println("                    |Jackpot Number = "+ lotValue+"|");
	            System.out.println("                    --------------------");
	            System.out.println("Player               Bet        OutCome        Winnings");
	            System.out.println("--");
	            synchronized (betManager) {
		            analyzeBet(lotValue);
	            }	            
	        } catch (InterruptedException ex) {
	        	System.out.println("Inturrept excepion");	            
	        }
		}
	}
		
	public void analyzeBet(Integer lotValue){
	  if(betManager.getActiveBets().size() > 0) {
    	for(BetObject betO : betManager.getActiveBets()) {
    		String outcome = "LOOSE";
    		Double winnings = 0.0;
    		if (betO.getBetOn().equals("EVEN") || betO.getBetOn().equals("ODD")) {
    			String betOn = betO.getBetOn();
    			if(betOn.equals("EVEN") && (lotValue % 2) == 0) {
    				outcome = "WIN";
    				winnings = betO.getAmount() * 2;
    			} else if(betOn.equals("ODD") && (lotValue % 2) != 0) {
    				outcome = "WIN";
    				winnings = betO.getAmount() * 2;
    			} else {
    				outcome= "LOOSE";
    			}
    		}else{ 
    			if(lotValue == Integer.parseInt(betO.getBetOn())) {
    				outcome = "WIN";
    				winnings = betO.getAmount() * 36;
    			}
    		}           		
    		System.out.print(betO.getUserName());            		
    		Util.alignSpaces(betO.getUserName() , 21);
    		System.out.print(betO.getBetOn());            		
    		Util.alignSpaces(String.valueOf(betO.getBetOn()),11);
    		System.out.print(outcome);
    		Util.alignSpaces(outcome,15);
    		System.out.println(winnings);
    		
    		userDAO.updateWinnings(betO.getUserName(), betO.getAmount(), winnings);
    	}	            	
    	betManager.clearActiveBets();
     }
	}
}	

