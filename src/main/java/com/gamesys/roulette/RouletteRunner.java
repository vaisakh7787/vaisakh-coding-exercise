package com.gamesys.roulette;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class RouletteRunner implements Runnable{
	public static final long RUNNER_INTERVAL = 30000L;
	public static final Integer MAX_BET = 36;
	public static final Integer MIN_BET = 1;
	BlockingQueue<Integer> runnerQueue = null;
	public static Long spinTimer = RUNNER_INTERVAL/1000; 
	int roundC = 1;
	
	public RouletteRunner(BlockingQueue<Integer> runnerQueue) {
		this.runnerQueue = runnerQueue;
	}
	
	public void run() {
		while(true) {
			spinTimer = RUNNER_INTERVAL/1000;
			int limit = spinTimer.intValue();
			try {				
				for (int i=limit;i>=0;i--) {
					Thread.sleep(1000);
					spinTimer--;
				}
				
				Random random = new Random();
				int randomNum = random.nextInt((MAX_BET - MIN_BET) + 1) + MIN_BET;
				System.out.println("                    --------------------");
	            System.out.println("                    |Round - "+ roundC++ +"       ");
				this.runnerQueue.put(randomNum);
				
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

}
