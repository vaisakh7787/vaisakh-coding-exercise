package com.gamesys.roulette;

public class RouletteUser {

	String userName;
	Double totalWin,totalBet;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Double getTotalWin() {
		return totalWin;
	}
	public void setTotalWin(Double totalWin) {
		this.totalWin = totalWin;
	}
	public Double getTotalBet() {
		return totalBet;
	}
	public void setTotalBet(Double totalBet) {
		this.totalBet = totalBet;
	}
	@Override
	public String toString() {
		return "Player Name - " + userName
				+ "\nTotalWin - " + totalWin
				+ "\nTotal Bet - " + totalBet;
	}	
	
	
}
