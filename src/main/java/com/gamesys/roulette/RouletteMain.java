/**
 * 
 */
package com.gamesys.roulette;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


/**
 * @author vaisakh
 *
 */
public class RouletteMain {
	static BlockingQueue<Integer> betResultQueue = new ArrayBlockingQueue<Integer>(1);
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("**********Welcome to ROULETTE********");
		
		RouletteDAO userDAO = RouletteDAO.getInstance();
		userDAO.initializeUsers();
		
		BetManager betManager = new BetManager(betResultQueue);
		betManager.startBets();
		
		RouletteRunner rRunner = new RouletteRunner(betResultQueue);
		Thread rRunnerT = new Thread(rRunner);
		rRunnerT.start();
		
		BetAnalyzer betA = new BetAnalyzer(betManager, betResultQueue);
		Thread betAT = new Thread(betA);
		betAT.start();		
	}

}
